"""do something"""

import json
import os
import requests

S3_BUCKET = os.environ['S3_BUCKET']


def handler(event, context):
    """do something"""
    print(event["body"])

    return {
        'statusCode': 200,
        'body': json.dumps({"success": "no action taken"}),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }
