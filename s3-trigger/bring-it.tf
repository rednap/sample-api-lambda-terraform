locals {
  s3_trigger_lambda_runtime = "python3.9"
  s3_trigger_zip_name = "s3_trigger.zip"
  s3_trigger_project_zip = "${path.cwd}/s3_trigger.zip"
  s3_trigger_lambda_bucket = "andy-klier"
  s3_trigger_source_bucket_name = "andy-klier.pypi"
  s3_trigger_lambda_name = "s3_trigger"
  s3_trigger_lambda_handler = "s3_trigger.handler"
  s3_trigger_lambda_log_name = "s3_trigger-lambda-logging"
  s3_trigger_lambda_role_name = "s3_trigger-lambda-function-lambda-role"
  s3_trigger_apigw_name = "s3_trigger-api-gw"
  s3_trigger_usage_plan_name = "s3_trigger-usage-plan"
  s3_trigger_api_key_name = "s3_trigger-key"
}

resource "aws_s3_object" "s3_trigger-lambda_object" {
  bucket = local.s3_trigger_lambda_bucket
  key    = local.s3_trigger_zip_name
  source = local.s3_trigger_zip_name
  etag = filemd5(local.s3_trigger_project_zip)
}

# LAMBDA FUNCTION

resource "aws_lambda_function" "s3_trigger-lambda-function" {
  function_name = local.s3_trigger_lambda_name
  s3_bucket = local.s3_trigger_lambda_bucket
  s3_key    = local.s3_trigger_zip_name
  handler = local.s3_trigger_lambda_handler
  runtime = local.s3_trigger_lambda_runtime
  memory_size = 256
  timeout     = 30
  role = aws_iam_role.s3_trigger-lambda_exec.arn
  source_code_hash = filebase64sha256(local.s3_trigger_project_zip)
  environment {
    variables = {
      ACCESS_ID = var.access_key
      ACCESS_KEY = var.secret_key
    }
  }
  depends_on    = [
    aws_iam_role_policy_attachment.s3_trigger-lambda_logs,
    aws_cloudwatch_log_group.s3_trigger-lambda-function-log-group,
    aws_s3_object.s3_trigger-lambda_object]
}

resource "aws_s3_bucket_versioning" "andy_klier_versioning" {
  bucket = aws_s3_bucket.s3_trigger_source_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_cors_configuration" "andy_klier_cors" {
  bucket = aws_s3_bucket.s3_trigger_source_bucket.bucket

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "PUT", "POST", "HEAD"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }
}

resource "aws_s3_bucket_acl" "andy_klier_bucket_acl" {
  bucket = aws_s3_bucket.s3_trigger_source_bucket.id
  acl    = "private"
}


resource "aws_s3_bucket_policy" "andy_klier_allow_access_from_another_account" {
  bucket = aws_s3_bucket.s3_trigger_source_bucket.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {   
            "Sid": "Stmt1446049153489",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::575296055612:user/andy"
            },  
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],  
            "Resource": "arn:aws:s3:::${local.s3_trigger_source_bucket_name}/*"
        }   
    ]   
}
  POLICY
}

resource "aws_s3_bucket" "s3_trigger_source_bucket" {
  force_destroy = true
  bucket = local.s3_trigger_source_bucket_name
}

# Permissions for lambda on create_thumbnail source bucket
resource "aws_lambda_permission" "s3_trigger_allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_trigger-lambda-function.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_trigger_source_bucket.arn
}

# IAM role which dictates what other AWS services the Lambda function
# may access.
resource "aws_iam_role" "s3_trigger-lambda_exec" {
  name = local.s3_trigger_lambda_role_name
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {   
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },  
      "Effect": "Allow",
      "Sid": ""
    }   
  ]
}
EOF
}

# CLOUDWATCH LOG GROUP

resource "aws_cloudwatch_log_group" "s3_trigger-lambda-function-log-group" {
  name              = "/aws/lambda/${local.s3_trigger_lambda_name}"
  retention_in_days = 14
}

resource "aws_iam_policy" "s3_trigger-lambda_logging" {
  name = local.s3_trigger_lambda_log_name
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s3_trigger-lambda_logs" {
  role = aws_iam_role.s3_trigger-lambda_exec.name
  policy_arn = aws_iam_policy.s3_trigger-lambda_logging.arn
  depends_on = [
    aws_iam_role.s3_trigger-lambda_exec
  ]
}

resource "aws_s3_bucket_notification" "s3_trigger_bucket_notification" {
  bucket = aws_s3_bucket.s3_trigger_source_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_trigger-lambda-function.arn
    events              = ["s3:ObjectCreated:*"]
#    filter_prefix       = "folder/"
  }
  depends_on = [aws_lambda_function.s3_trigger-lambda-function]
}

# OUTPUTS

output "source-bucket" {
  value = aws_s3_bucket.s3_trigger_source_bucket.id
}
