terraform {
  backend "s3" {
    bucket  = "sample-api-lambda-terraform"
    key     = "version2.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

