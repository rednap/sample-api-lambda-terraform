# Sample Terraform for API Gateway (POST) with a Lambda Handler (Python)  
  
All infra code is up to date with: `v1.1.7`   

There is a tiny sample python lambda function with dependencies in `python-app`.   
For the sake of ease of use, i've included a zipped package.   
   
Copy `terraform.tfvars.default` to `terraform.tfvars` and add your AWS ID and KEY.   
Edit `alfa.tf` and change region.   
Edit `beta.tf` and set your aws details, state file, etc.   
Edit `bring-it.tf` and change local variable `sample_terraform_lambda_bucket` at least.   


`$ terraform init`  
  
`$ terraform plan`   
   
`$ terraform apply`   
    
example of invocation with curl (actual api key and hostname will be output by terraform `terraform output -json`):   
`curl -H 'X-Api-Key: orh0ejwOJ56dnToSmOZZv8r8aiIjhKRm1FLUYMWU' -X POST -d '{"earth":"people"}' https://DESTROYED.execute-api.us-east-1.amazonaws.com/api/go`
```
{"success": "no action taken"}
```   
   
Replace all occurances of `sample_terraform` with anything:   
`sed -i 's/sample_terraform/anything/g' bring-it.tf` - linux   
`sed -i '.bak' 's/sample_terraform/anything/g' bring-it.tf` - mac/bsd    
   
NOTE: test infra is only deployed for testing
