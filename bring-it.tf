locals {
  sample_terraform_lambda_runtime = "python3.9"
  sample_terraform_zip_name = "python-app.zip"
  sample_terraform_project_zip = "${path.cwd}/python-app.zip"
  sample_terraform_lambda_bucket = "sample-api-lambda-terraform"
  sample_terraform_lambda_name = "sample_terraform"
  sample_terraform_lambda_handler = "python-app.handler"
  sample_terraform_lambda_log_name = "sample_terraform-lambda-logging"
  sample_terraform_lambda_role_name = "sample_terraform-lambda-function-lambda-role"
  sample_terraform_apigw_name = "sample_terraform-api-gw"
  sample_terraform_usage_plan_name = "sample_terraform-usage-plan"
  sample_terraform_api_key_name = "sample_terraform-key"
}

resource "aws_s3_object" "sample_terraform-lambda_object" {
  bucket = local.sample_terraform_lambda_bucket
  key    = local.sample_terraform_zip_name
  source = local.sample_terraform_zip_name
  etag = filemd5(local.sample_terraform_project_zip)
}

# LAMBDA FUNCTION

resource "aws_lambda_function" "sample_terraform-lambda-function" {
  function_name = local.sample_terraform_lambda_name
  s3_bucket = local.sample_terraform_lambda_bucket
  s3_key    = local.sample_terraform_zip_name
  handler = local.sample_terraform_lambda_handler
  runtime = local.sample_terraform_lambda_runtime
  memory_size = 256
  timeout     = 30
  role = aws_iam_role.sample_terraform-lambda_exec.arn
  source_code_hash = filebase64sha256(local.sample_terraform_project_zip)
  environment {
    variables = {
      S3_BUCKET = "example"
    }
  }
  depends_on    = [
    aws_iam_role_policy_attachment.sample_terraform-lambda_logs,
    aws_cloudwatch_log_group.sample_terraform-lambda-function-log-group,
    aws_s3_object.sample_terraform-lambda_object]
}

# IAM role which dictates what other AWS services the Lambda function
# may access.
resource "aws_iam_role" "sample_terraform-lambda_exec" {
  name = local.sample_terraform_lambda_role_name
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {   
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },  
      "Effect": "Allow",
      "Sid": ""
    }   
  ]
}
EOF
}

# CLOUDWATCH LOG GROUP

resource "aws_cloudwatch_log_group" "sample_terraform-lambda-function-log-group" {
  name              = "/aws/lambda/${local.sample_terraform_lambda_name}"
  retention_in_days = 14
}

resource "aws_iam_policy" "sample_terraform-lambda_logging" {
  name = local.sample_terraform_lambda_log_name
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "sample_terraform-lambda_logs" {
  role = aws_iam_role.sample_terraform-lambda_exec.name
  policy_arn = aws_iam_policy.sample_terraform-lambda_logging.arn
  depends_on = [
    aws_iam_role.sample_terraform-lambda_exec
  ]
}

# API GATEWAY

resource "aws_api_gateway_rest_api" "sample_terraform-api" {
  name = local.sample_terraform_apigw_name
#  binary_media_types = [
#    "application/zip",
#    "application/octet-stream",
#    "*/*"
#  ]
}

# API KEY/USAGE PLAN

resource "aws_api_gateway_usage_plan" "sample_terraform-usage_plan" {
  name = local.sample_terraform_usage_plan_name
  api_stages {
    api_id = aws_api_gateway_rest_api.sample_terraform-api.id
    stage  = "api"
  }
  quota_settings {
    limit  = 5000
    period = "MONTH"
  }
  throttle_settings {
    burst_limit = 200
    rate_limit  = 100
  }
  depends_on = [
    aws_api_gateway_rest_api.sample_terraform-api,
    aws_api_gateway_deployment.sample_terraform-deploy,
  ]
}

# api key

resource "aws_api_gateway_api_key" "sample_terraform-key" {
  name = local.sample_terraform_api_key_name
}
 
resource "aws_api_gateway_usage_plan_key" "sample_terraform-plan_key" {
  key_id        = aws_api_gateway_api_key.sample_terraform-key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.sample_terraform-usage_plan.id
  depends_on    = [aws_api_gateway_usage_plan.sample_terraform-usage_plan]
}

# CUSTOMIZE ROUTES BELOW

module "sample_terraform-route" {
  source        = "./serverless-resource"
  api           = aws_api_gateway_rest_api.sample_terraform-api.id
  root_resource = aws_api_gateway_rest_api.sample_terraform-api.root_resource_id
#  api_key_required = false
  api_key_required = true
  resource      = "go"
  num_methods   = 1
  methods = [
    {
      method     = "POST"
      type       = "AWS_PROXY"
      invoke_arn = aws_lambda_function.sample_terraform-lambda-function.invoke_arn
    },
  ]
}

# GET example below
#module "sample_terraform-route" {
#  source        = "./serverless-resource"
#  api           = aws_api_gateway_rest_api.sample_terraform-api.id
#  root_resource = module.sample_terraform-route.resource
#  api_key_required = true
#  resource      = "{slug}"
#  num_methods   = 1
#  methods = [
#    {
#      method     = "GET"
#      type       = "AWS_PROXY"
#      invoke_arn = aws_lambda_function.sample_terraform-lambda-function.invoke_arn
#    },
#  ]
#}

# DEPLOYMENT

resource "aws_api_gateway_deployment" "sample_terraform-deploy" {
  depends_on  = [module.sample_terraform-route]
  rest_api_id = aws_api_gateway_rest_api.sample_terraform-api.id
  stage_name  = "api"
  variables = {
    deploy_version = "0.0.1"
  }
}

# PERMISSION FOR API/LAMBDA

resource "aws_lambda_permission" "sample_terraform-apigw" {
  statement_id  = "Allow-sample_terraform-sample_terraform-apiInvoke"
  action        = "lambda:InvokeFunction"
  function_name = local.sample_terraform_lambda_name
  principal     = "apigateway.amazonaws.com"
  depends_on = [aws_lambda_function.sample_terraform-lambda-function]
  source_arn = "${aws_api_gateway_rest_api.sample_terraform-api.execution_arn}/*/*/*"
}


# OUTPUTS

output "sample_terraform-endpoint" {
  value = aws_api_gateway_deployment.sample_terraform-deploy.invoke_url
}

output "sample_terraform-api_key" {
  value = aws_api_gateway_api_key.sample_terraform-key.value
  sensitive = true
}
